# Example Anchors Tabular Explanation for Income Prediction using scikit-learn

This example uses a [US income dataset](https://archive.ics.uci.edu/ml/datasets/adult)

> An example input to interact with the model can be found in `input.json`

We can create a InferenceService with a trained sklearn predictor for this dataset and an associated model explainer. The black box explainer algorithm we will use is the Tabular version of Anchors from the [Alibi open source library](https://github.com/SeldonIO/alibi). More details on this algorithm and configuration settings that can be set can be found in the [Seldon Alibi documentation](https://docs.seldon.io/projects/alibi/en/stable/).

Pretrained model and explainer artefacts are available in the Deeploy contract folders `model/` and `explainer/`

Note: Make sure to ensure that the libraries used are the ones with our recommended versions.


